SHELL := /bin/bash # Use bash syntax
# XLEN=64 
CUR = ./
bspinc = ./include
bspcore = ./core
bspboard = ./board/$(TARGET)
gen_lib = ./output/gen_lib
# PROGRAM ?=all
# TARGET ?=artix7_35t
all: check create_dir
	make main

check:
	
ifeq ($(TARGET),parashu)  # e-class 100t rv32imac
	@echo $(filepath)
	@$(eval XLEN = 32)
	@$(eval WIDTH = 4)
	@$(eval FLAGS = -D parashu )
	@$(eval MARCH=rv32imac)
	@$(eval MABI=ilp32)
	@$(eval UPLOADER = $(UPLOADER)/spansion)
	@$(eval INTERFACE=$(bspboard)/ftdi)
	@$(eval DC=$(FLAGS) $(D))
else
ifeq ($(TARGET),vajra)  # c-class 100t rv64imacsu
	@echo $(filepath)
	@$(eval XLEN = 64)
	@$(eval WIDTH = 8 )
	@$(eval FLAGS = -D vajra )
	@$(eval MARCH=rv64imac)
	@$(eval MABI=lp64)
	@$(eval UPLOADER= $(UPLOADER)/spansion)
	@$(eval INTERFACE = $(bspboard)/ftdi)
	@$(eval DC=$(FLAGS) $(D))
else
ifeq ($(TARGET),sos)                         # c-class 100t rv64imacsu
	@$(eval XLEN = 64)
	@$(eval WIDTH = 8)
	@$(eval FLAGS= -D SOS)
	@$(eval MARCH=rv64imac)
	@$(eval MABI=lp64)
	@$(eval UPLOADER=$(UPLOADDIR)/spansion)
	@$(eval INTERFACE=$(bspboard)/ftdi)
	@$(eval DC =$(FLAGS) $(D)) 
else

	@echo -e "$(TARGET) not supported"
endif
endif
endif


main: check create_dir
	@echo "Compiling the main"
	@riscv$(XLEN)-unknown-elf-g++ -march=$(MARCH) -mabi=$(MABI) $(DC) -w  -mcmodel=medany -static -std=gnu99 -fno-builtin-printf -I$(bspinc) -I$(bspboard) -c ./main.cpp -o ./output/main.o -lm -lgcc
	
	
	@echo "Compilation started"
	@riscv$(XLEN)-unknown-elf-gcc -march=$(MARCH) -mabi=$(MABI) $(DC) -mcmodel=medany -static -std=gnu99 -fno-common -fno-builtin-printf -D__ASSEMBLY__=1 -I$(bspinc) -c $(bspcore)/start.S -o $(gen_lib)/start.o
	@riscv$(XLEN)-unknown-elf-gcc -march=$(MARCH) -mabi=$(MABI) $(DC) -mcmodel=medany -static -std=gnu99 -fno-common -fno-builtin-printf -D__ASSEMBLY__=1 -I$(bspinc) -c $(bspcore)/trap.S -o $(gen_lib)/trap.o
	@riscv$(XLEN)-unknown-elf-gcc -march=$(MARCH) -mabi=$(MABI) $(DC) -mcmodel=medany -static -std=gnu99 -fno-builtin-printf -I$(bspinc) -I$(bspboard) -c $(bspcore)/init.c -o $(gen_lib)/init.o -lm -lgcc
	@riscv$(XLEN)-unknown-elf-gcc -march=$(MARCH) -mabi=$(MABI) $(DC) -mcmodel=medany -static -std=gnu99 -fno-builtin-printf -I$(bspinc) -I$(bspboard) -c $(bspcore)/traps.c -o $(gen_lib)/traps.o -lm -lgcc
	@riscv$(XLEN)-unknown-elf-gcc -march=$(MARCH) -mabi=$(MABI) $(DC) -w  -mcmodel=medany -static -std=gnu99 -fno-builtin-printf -I$(bspinc) -I$(bspboard) -c ./uart.c -o $(gen_lib)/uart.o -lm -lgcc
	@riscv$(XLEN)-unknown-elf-gcc -march=$(MARCH) -mabi=$(MABI) $(DC) -w  -mcmodel=medany -static -std=gnu99 -fno-builtin-printf -I$(bspinc) -I$(bspboard) -c ./printf.c -o $(gen_lib)/printf.o -lm -lgcc
	@riscv$(XLEN)-unknown-elf-g++ -march=$(MARCH) -mabi=$(MABI) $(DC) -w  -mcmodel=medany -static -std=gnu99 -fno-builtin-printf -I$(bspinc) -I$(bspboard) -c ./String.cpp -o $(gen_lib)/String.o -lm -lgcc
	@riscv$(XLEN)-unknown-elf-ar rcs $(gen_lib)/core.a $(gen_lib)/start.o
	@riscv$(XLEN)-unknown-elf-ar rcs $(gen_lib)/core.a $(gen_lib)/trap.o
	@riscv$(XLEN)-unknown-elf-ar rcs $(gen_lib)/core.a $(gen_lib)/init.o
	@riscv$(XLEN)-unknown-elf-ar rcs $(gen_lib)/core.a $(gen_lib)/traps.o 
	@riscv$(XLEN)-unknown-elf-ar rcs $(gen_lib)/core.a $(gen_lib)/uart.o
	@riscv$(XLEN)-unknown-elf-ar rcs $(gen_lib)/core.a $(gen_lib)/printf.o
	@riscv$(XLEN)-unknown-elf-ar rcs $(gen_lib)/core.a $(gen_lib)/String.o


#Taken from Arduino IDE 
	@riscv$(XLEN)-unknown-elf-g++ -T $(bspboard)/link.ld -nostartfiles -Wl,-N ./output/main.o   -nostdlib -Wl,--start-group $(gen_lib)/core.a -lm -lstdc++ -lc -lgloss -Wl,--end-group -lgcc -o ./output/main.shakti -mcmodel=medany  -march=$(MARCH) -mabi=$(MABI) -static -nostartfiles -lm -lgcc

	@riscv$(XLEN)-unknown-elf-objdump -D ./output/main.shakti &> ./output/main.dump
# @g++ -march=rv32imac  -w   -static -std=gnu99 -fno-builtin-printf -c ./main.cpp -o ./output/main.o -lm -lgcc




create_dir:
	@mkdir -p ./output
	@mkdir -p ./output/gen_lib

clean: 
	rm -r ./output

temp:
	# @riscv$(XLEN)-unknown-elf-g++ -march=$(MARCH) -mabi=$(MABI) $(DC) -mcmodel=medany -static -std=gnu99 -fno-common -fno-builtin-printf -D__ASSEMBLY__=1 -I$(bspinc) -c $(bspcore)/start.S -o ./output/start.o
	# @riscv$(XLEN)-unknown-elf-g++ -march=$(MARCH) -mabi=$(MABI) $(DC) -mcmodel=medany -static -std=gnu99 -fno-common -fno-builtin-printf -D__ASSEMBLY__=1 -I$(bspinc) -c $(bspcore)/trap.S -o ./output/trap.o
	# @riscv$(XLEN)-unknown-elf-g++ -march=$(MARCH) -mabi=$(MABI) $(DC) -mcmodel=medany -static -std=gnu99 -fno-builtin-printf -I$(bspinc) -I$(bspboard) -c $(bspcore)/traps.c -o ./output/traps.o -lm -lgcc
	# @echo "Hello 1"
	# @riscv$(XLEN)-unknown-elf-g++ -march=$(MARCH) -mabi=$(MABI) $(DC) -mcmodel=medany -static -std=gnu99 -fno-builtin-printf -I$(bspinc) -I$(bspboard) -c $(bspcore)/init.c -o ./output/init.o -lm -lgcc
	# @echo "Hello"
	# @riscv$(XLEN)-unknown-elf-g++ -march=$(MARCH) -mabi=$(MABI) $(DC) -w  -mcmodel=medany -static -std=gnu99 -fno-builtin-printf -I$(bspinc) -I$(bspboard) -c ./uart.c -o ./output/uart.o -lm -lgcc
	# @riscv$(XLEN)-unknown-elf-g++ -march=$(MARCH) -mabi=$(MABI) $(DC) -w  -mcmodel=medany -static -std=gnu99 -fno-builtin-printf -I$(bspinc) -I$(bspboard) -c ./printf.c -o ./output/printf.o -lm -lgcc
	# @riscv$(XLEN)-unknown-elf-g++ -march=$(MARCH) -mabi=$(MABI) $(DC) -w  -mcmodel=medany -static -std=gnu99 -fno-builtin-printf -I$(bspinc) -I$(bspboard) -c ./String.cpp -o ./output/String.o -lm -lgcc
	# @riscv$(XLEN)-unknown-elf-gcc -march=$(MARCH) -mabi=$(MABI) $(DC) -w  -mcmodel=medany -static -std=gnu99 -fno-builtin-printf -I$(bspinc) -I$(bspboard) -c ./main.cpp -o ./output/main.o -lm -lgcc

	# @riscv64-unknown-elf-g++ -T ./link.ld ./output/*.o   -o ./output/main.shakti -mcmodel=medany  -march=rv64imac -mabi=lp64 -static -nostartfiles -lm -lgcc

	# @riscv64-unknown-elf-g++ -T ./link.ld ./output/String.o ./output/printf.o ./output/uart.o ./output/main.o   -o ./output/main.shakti -mcmodel=medany  -march=rv64imac -mabi=lp64 -static -nostartfiles -lm -lgcc
