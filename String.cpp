#include <String.h> 
#include <stdint.h>
extern "C" { 
	#include "uart.h"
}
#define LEDS GPIO16 

 String :: String(void)
{
    // printf("\nString Constructor");
	write_word(GPIO_DIRECTION_CNTRL_REG,GPIO17); //LD 5
	write_word(GPIO_DATA_REG,0x0);

	write_word(GPIO_DATA_REG,GPIO17);
	printf("\nString void");
	char_temp = 'z'; //z representing NULL

}

 String :: String(char c)
{   
	write_word(GPIO_DIRECTION_CNTRL_REG,LEDS); //LD 4
	write_word(GPIO_DATA_REG,0x0);
	write_word(GPIO_DATA_REG,LEDS);
	char_temp = c;
    printf("\nString character Constructor %c",c);

}

void String :: print_s(void)
{
    write_word(GPIO_DIRECTION_CNTRL_REG, GPIO16 | GPIO17);
    write_word(GPIO_DATA_REG,0x0);
	if(char_temp)
    	printf("\nString Print function %c",char_temp);
	else 
		printf("\nString Print function char_temp - NULL");
}

unsigned long String :: read_word(uint32_t *addr)
{
	// log_debug("addr = %x data = %x\n", addr, *addr);
	return *addr;
}

/** @fn void write_word(int *addr, unsigned long val)
 * @brief  writes a value to an address
 * @param int*
 * @param unsigned long
 */
void String :: write_word(uint32_t *addr, unsigned long val)
{
	*addr = val;
	// log_debug("addr = %x data = %x\n", addr, *addr);
}
