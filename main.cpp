// #include <iostream>
#include <String.h> 
using namespace std;
extern "C"{
#include "uart.h"
}

// Calling constructor globally 
String str1('a');
String str3;

int main(void)
{

    printf("\n\tCPP Testing");
    
    // putchar('a');
    String str2('b');
    String str4;
 
    printf("\n Print data in str1- Global constructor");
    str1.print_s();
    printf("\n\n Print data in str2");
    str2.print_s();
    printf("\n\n Print data in str3- Global constructor");
    str3.print_s();
    printf("\n\n Print data in str4");
    str4.print_s();
    //  abc.printf();
    // cout << "Test StrinSg" << endl;
    while(1);

}